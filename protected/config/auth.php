<?php
return array(
	'deleted' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Deleted',
		'bizRule' => null,
		'data' => null
	),
	'guest' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule' => null,
		'data' => null
	),
);