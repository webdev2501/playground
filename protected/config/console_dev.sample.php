<?php
return array(
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=playground_udimi',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
			'enableProfiling' => true,
			'enableParamLogging' => true,
			'attributes'=>array(
				PDO::MYSQL_ATTR_LOCAL_INFILE => true,
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
			),
		),
	),
	'params'=>array(
		'is_local' => true,
		'site_host' => 'http://playground.udimi.loc',
	),
);