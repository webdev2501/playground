<!DOCTYPE html>
<html>
<head>
	<?php
	$cs = Yii::app()->clientScript;
	$cs->registerMetaTag('text/html; charset=utf-8', null, 'Content-Type');
	$cs->registerMetaTag($this->metaKeywords, 'Keywords');
	$cs->registerMetaTag($this->metaDescription, 'Description');

	$cs->registerMetaTag(Yii::app()->getLanguage(), 'language');
	if ($this->favicon) echo CHtml::linkTag('shortcut icon', 'image/png', $this->favicon);

	if (isset($this->layoutPackageName))
		$this->package();

	$cs->registerScript('initApplication', "appMain.initApp(".CJavaScript::encode($this->jsAppOptions).");", CClientScript::POS_READY);

	if (!$this->skipFbOgTags) $this->fbOgTags();
	?>
	<title><?= $this->metaTitle; ?></title>

</head>
<body>

	<div class="app-layouts-main-navigation messages-fixed-panel">
		<div class="inner">
			<div class="logo-container">
				<div class="logo js-tut-logo">
					<?php $this->widget('w.wMenuLogo.wMenuLogo') ?>
				</div>
			</div>
			<div class="menu-container">
				<?php if (!$this->skipTopMenu): $this->widget('w.wMenuTop.wMenuTop', array('layoutPreviewMode' => $this->layoutPreviewMode)); endif; ?>
			</div>
		</div>
	</div>

	<div class="app-layouts-main-page-container messages-fixed-panel">
		<div class="float" id="layout_container_global" data-pagecontainer="true">
			<?= $content ?>
		</div>
		<div class="clearfix"></div>
	</div>

	<?php $this->widget('w.wFlashMessages.wFlashMessages', array(
		'options'=>$this->jsAppOptions['toastMsgConfig']
	)); ?>

	<div id="modal_dialog_general" class="modal fade"  tabindex="-1">
		<div class="modal-dialog">
			<span class="title-close" data-dismiss="modal">Close</span>
			<div class="modal-content"></div>
		</div>
	</div>

</body>
</html>