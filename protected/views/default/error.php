<?php
	$error = MyUtils::getFirstError($model, true);
	try {
		$isBrokenLink = $model->getFindedBrokenLink($error['attribute']);
		foreach ($model->isSkipBl as $attribute) {
			if (!$isBrokenLink || $attribute!=$error['attribute'])
				echo CHtml::activeHiddenField($model, "isSkipBl[]", ['value'=>$attribute,  'id'=>false]);
		}
	} catch (CException $e) {
		$isBrokenLink = false;
	}
?>
<?php if ($isBrokenLink): ?>
	<div class="alert alert-warning">
		<strong>Warning!</strong> <?= $error['error']; ?>
		<div class="b-error-description">
			<div class="checkbox checkbox-primary">
				<?= CHtml::activeCheckBox($model, "isSkipBl[]", ['value'=>$error['attribute'], 'uncheckValue'=>null, 'checked'=>in_array($error['attribute'], $model->isSkipBl)]) ?>
				<label class="checkbox-skip-color" for="<?=get_class($model);?>_isSkipBl">
					Use the link in <?=$model->getAttributeLabel($error['attribute']);?> anyway
				</label>
			</div>
		</div>
	</div>
<?php else: ?>
	<div class="alert alert-danger">
		<?= $error['error']; ?>
	</div>
<?php endif ?>