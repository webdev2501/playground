<?php
$this->metaTitle = 'Udimi. Sign Up';
$this->jsInit('appSite', 'initSignup');
?>

<div class="app-site-signup">
	<div class="b-signup-form">

		<div class="b-title">Sign Up</div>

		<?php $form = $this->beginWidget('CActiveForm', [
			'id' => 'register-form',
			'htmlOptions' => [
				'class' => 'form-horizontal',
			],
		]); ?>

		<div class="form-group">
			<div class="col-sm-12">
				<?= $form->textField($model, 'fullname', ['class' => 'form-control', 'placeholder'=>'Name']); ?>
				<div class="e-err"><?= $model->getError('fullname') ?></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
				<?= $form->textField($model, 'email', ['class' => 'form-control', 'placeholder'=>'Email']); ?>
				<div class="e-err"><?= $model->getError('email') ?></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
				<?= $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder'=>'Password', 'autocomplete' => 'off')); ?>
				<div class="e-err"><?= $model->getError('password') ?></div>
			</div>
		</div>

		<div class="b-btn">
			<button type="submit" class="btn btn-modern-primary ajax-post">Sign Up</button>
		</div>

		<?php $this->endWidget() ?>
	</div>
</div>