<?php
$this->metaTitle = 'Udimi. Login';
$this->jsInit('appSite', 'initLogin');
?>

<div class="app-site-auth">

	<div class="b-title">
		<div class="b-col-left">
			Login
		</div>
		<div class="b-col-right">
			<a href="<?php echo $this->createUrl('/site/forgot') ?>" class="ajax-get">Forgot password?</a>
		</div>
	</div>

	<div class="e-line"></div>

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'login-form',
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			),
		)
	); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'username', array('class' => 'control-label col-sm-3 required-hide')); ?>
		<div class="col-sm-9">
			<?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'password', array('class' => 'control-label col-sm-3 required-hide')); ?>
		<div class="col-sm-9">
			<?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
			<div class="e-err"><?= MyUtils::getFirstError($model) ?></div>
		</div>
	</div>

	<?php if (CCaptcha::checkRequirements() && LoginsLog::isCaptchaShown()): ?>
		<div class="form-group captha-group">
			<div class="col-sm-9 col-sm-offset-3">
				<?php $this->widget('CCaptcha') ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'verifyCode', array('class' => 'control-label col-sm-3 required-hide')); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model, 'verifyCode', array('class' => 'form-control')) ?>
			</div>
		</div>
	<?php endif ?>

	<div class="b-btn m-sign">
		<button type="submit" class="btn btn-modern-primary ajax-post">Login</button>
	</div>

	<?php $this->endWidget() ?>

</div>