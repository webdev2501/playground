module.exports = {
	fontelloUpdate: {
		options: {
			config: '../httpdocs/media/libs/fontello/config.json',
			css: '../httpdocs/media/libs/fontello/css',
			fonts: '../httpdocs/media/libs/fontello/font',
		},
	},
};