<?php

class EmailCategory extends ActiveRecord
{
	const SELLER = 2;

	public function tableName()
	{
		return 'email_category';
	}

	public function relations()
	{
		return array(
			'idType' => array(self::HAS_MANY, 'EmailType', 'id_category'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function loadStructure($id_user)
	{
		return self::model()
			->with(array(
			'idType'=>array('condition'=>'idType.editable=1'),
			'idType.idBlocked'=>array('on'=>'idBlocked.id_user=:id_user'),
		))
			->findAll(array(
			'order'=>'t.ord ASC, idType.ord ASC',
			'params'=>array(':id_user'=>$id_user)
		));
	}

}