<?php
class Ip2ProxyManual extends ActiveRecord
{
	const REASON_MANUAL = 'manual';
	const REASON_TOR = 'tor';
	const REASON_TOR_EXIT = 'tor_exit';
	const REASON_WEB_PROXY = 'webproxy';
	const REASON_WEBRTC_LEAK = 'webrtc';

	public function tableName()
	{
		return 'ip2_proxy_manual';
	}

	public function rules()
	{
		return array(
			array('ip', 'required'),
			array('ip', 'IpV4'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}