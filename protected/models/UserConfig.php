<?php
class UserConfig extends ActiveRecord
{
	public function tableName()
	{
		return 'user_config';
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
