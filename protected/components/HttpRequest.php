<?php
class HttpRequest extends CHttpRequest
{
	public $noCsrfValidationRoutes=array();

	private $_requestUri;

	protected function normalizeRequest()
	{
		//attach event handlers for CSRFin the parent
		parent::normalizeRequest();

		//remove the event handler CSRF if this is a route we want skipped
		if($this->enableCsrfValidation)
		{
			$url=Yii::app()->getUrlManager()->parseUrl($this);
			foreach($this->noCsrfValidationRoutes as $route)
			{
				if(strpos($url,$route)===0)
					Yii::app()->detachEventHandler('onBeginRequest',array($this,'validateCsrfToken'));
			}
		}
	}

	public function validateCsrfToken($event)
	{
		if ($this->getIsPostRequest() ||
			$this->getIsPutRequest() ||
			$this->getIsDeleteRequest())
		{
			$cookies=$this->getCookies();

			$method=$this->getRequestType();
			switch($method)
			{
				case 'POST':
					$userToken=$this->getPost($this->csrfTokenName);
					break;
				case 'PUT':
					$userToken=$this->getPut($this->csrfTokenName);
					break;
				case 'DELETE':
					$userToken=$this->getDelete($this->csrfTokenName);
			}

			if (!empty($userToken) && $cookies->contains($this->csrfTokenName))
			{
				$cookieToken=$cookies->itemAt($this->csrfTokenName)->value;
				$valid=$cookieToken===$userToken;
			}
			else
				$valid = false;

//				if (!$valid) {
//					$cookie = $cookies->contains($this->csrfTokenName) ? $cookies->itemAt($this->csrfTokenName)->value : 'none';
//					Yii::log("CSRF token \n\nuserToken: ".print_r($userToken, true)." \ncookieToken: ".print_r($cookie, true)." \n\nServer:".print_r($_SERVER, true)." \n\nCookie:".print_r($_COOKIE, true)." \n\nPost:".print_r($_POST, true),
//						'warning', 'monitoring');
//
//					throw new CHttpException(400, 'The CSRF token could not be verified.'));
//				}
		}
	}

	public function isModal()
	{
		if (isset($_SERVER['HTTP_IS_MODAL_REQUEST'])) {
			return $_SERVER['HTTP_IS_MODAL_REQUEST'];
		} else {
			return false;
		}
	}

	public function getOriginalRoute()
	{
		if (isset($_SERVER['HTTP_ORIGINAL_ROUTE'])) {
			return $_SERVER['HTTP_ORIGINAL_ROUTE'];
		} else {
			return false;
		}
	}

	public function getIdUser()
	{
		if (isset($_SERVER['HTTP_ID_USER'])) {
			return $_SERVER['HTTP_ID_USER'];
		} else {
			return false;
		}
	}

	public function currentLayout()
	{
		if (isset($_SERVER['HTTP_CURRENT_LAYOUT'])) {
			return mb_stristr($_SERVER['HTTP_CURRENT_LAYOUT'], '-ajax', true) ? mb_stristr($_SERVER['HTTP_CURRENT_LAYOUT'], '-ajax', true) : $_SERVER['HTTP_CURRENT_LAYOUT'];
//			return $_SERVER['HTTP_CURRENT_LAYOUT'];
		} else {
			return false;
		}
	}

	public function getResourcesVersion()
	{
		if (isset($_SERVER['HTTP_RESOURCES_VERSION'])) {
			return $_SERVER['HTTP_RESOURCES_VERSION'];
		} else {
			return false;
		}
	}

	public function getIsMatchMasksList()
	{
		if (isset($_SERVER['HTTP_IS_MATCH_MASKS_LIST'])) {
			return $_SERVER['HTTP_IS_MATCH_MASKS_LIST'];
		} else {
			return false;
		}
	}

	public function getPost($name, $defaultValue=null)
	{
		return is_array($name) ? (isset($_POST[$name[0]][$name[1]]) ? $_POST[$name[0]][$name[1]] : $defaultValue) : parent::getPost($name, $defaultValue);
	}

	public function getUrl()
	{
		return $this->getRequestUri();
	}

	public function getRequestUri()
	{
		if ($this->_requestUri === null)
			$this->_requestUri = MultilangHelper::processLangInUrl(parent::getRequestUri());

		return $this->_requestUri;
	}

	public function getOriginalUrl()
	{
		return $this->getOriginalRequestUri();
	}

	public function getOriginalRequestUri()
	{
		return MultilangHelper::addLangToUrl($this->getRequestUri());
	}
}