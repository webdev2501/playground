<?php
class IpV4 extends CValidator
{
	protected function validateAttribute($object, $attribute)
	{
		if (filter_var($object->$attribute, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false) {
			$message=$this->message!==null ? $this->message:Yii::t('app', 'IP address {attribute} has wrong format');
			$this->addError($object, $attribute, $message);
		}
	}
}