<?php

class wGtitle extends CWidget
{
	public $left;
	public $right;

	public function init()
	{
		$this->_build('left');
		$this->_build('right');
	}

	public function run()
	{
		$this->render('index');
	}

	private function _build($name)
	{
		if (!is_array($this->{$name})) {
			$tmp = $this->{$name};
			$this->{$name} = array();
			$this->{$name}['content'] = $tmp;
		}
		if (empty($this->{$name}['tag'])) {
			$this->{$name}['tag'] = 'span';
		}
		if (empty($this->{$name}['htmlOptions'])) {
			$this->{$name}['htmlOptions']['class'] = 'l-title-default';
		}
		$this->{$name} = CHtml::tag($this->{$name}['tag'], $this->{$name}['htmlOptions'], $this->{$name}['content']);
	}
}