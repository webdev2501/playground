<?php

class wSimpleDrop extends CWidget
{
	public $name;
	public $select;
	public $data=[];
	public $htmlOptions=[];
	public $dropPosition='left';
	public $hideCaret=false;

	public function run()
	{
		$class = ['app-widget-simple-drop'];
		if (isset($this->htmlOptions['class'])) {
			$class[] = $this->htmlOptions['class'];
		}
		$this->htmlOptions['class'] = implode(' ', $class);

		if (isset($this->data[$this->select])) {
			$selectedLabel = $this->data[$this->select];
		} else {
			$arr = array_values($this->data);
			if (isset($arr[0])) {
				$selectedLabel = $arr[0];
			} else {
				$selectedLabel = 'None';
			}
		}

		$this->render('index', ['selectedLabel'=>$selectedLabel]);
	}
}