function AppWidgetLoadMore()
{
	this.options = {}

	this.nextPageUrl = false;

	//this.progressBarId;

	this.progressBarDefaultHtml;
	this.pause = false;

	var self = this;

	this.init = function(options)
	{
		this.options = {}
		this.nextPageUrl = false;

		appUtils.extend(self.options, options);

		if (typeof self.options.progressBarSelector == 'undefined') self.buildProgressBar();

		if (typeof self.options.nextPageUrl !== 'undefined') self.nextPageUrl = self.options.nextPageUrl;

		if (self.options.mode == 'load-more-link' || self.options.mode == 'load-more-scroll') {
			$('#'+self.options.linkContainerId+' a').click(function(e) {
				if (!self.options.block || self.options.mode == 'load-more-link') {
					// блокируем ajax подгрузку
					self.options.block = true;

					self.showProgressBar();

					if (self.options.isHistoryChanged)
						appMain.ajaxLoadUrlHistory(self.getUrl());
					else
						appMain.ajaxLoadUrl(self.getUrl());
				}

				e.preventDefault();
			});
		}

		// если включен режим загрузки по скроллу и нет блокировки
		if ((self.options.mode == 'load-more-scroll' || self.options.mode == 'load-more-scroll-floatpager') && !self.options.block)
		{

			//$('#' + self.options.linkContainerId + ' a').click(function (e) {
			//	$('body, html').animate({scrollTop: 0}, 0);
			//})
			$(window).on('scroll.wloadmore', function(){
				// инициализация пагинатора при скролле окна браузера
				if (self.options.mode == 'load-more-scroll-floatpager') {
					self.buildPagination();
				}

				// если мы достигли конца документа и нет блокировки - грузим контент
				if (($(window).height()+$(window).scrollTop() >= $(document).height()-100) && (!self.options.block) && (!self.pause))
				{
					// блокируем ajax подгрузку
					self.options.block = true;

					// постраничка превращается в индикатор загрузки
					self.showProgressBar();

					// грузим контент
					appMain.ajaxLoadUrl(self.getUrl());
				}
			});
		}
	}

	this.buildProgressBar = function()
	{
		this.progressBarId = self.options.linkContainerId + 'progressbar';

		var bar = $('<div/>', {
			id: this.progressBarId,
			class: 'load-more-progress-bar hidden',
			html: '<i class="icon-upload"></i>'
		});
		if (this.options.reverse) {
			bar.insertBefore(this.options.container);
		} else {
			bar.insertAfter(this.options.container);
		}
	}

	this.showProgressBar = function()
	{
		if (self.options.progressBarShowCb) {
			eval( self.options.progressBarShowCb )();
		} else if (typeof self.options.progressBarSelector == 'undefined') {
			$('#'+this.progressBarId).removeClass('hidden');
		} else {
			self.progressBarDefaultHtml = $(self.options.progressBarSelector).html();
			$(self.options.progressBarSelector).html('<i class="icon-upload"></i>');
		}

	}

	this.hideProgressBar = function()
	{
		if (self.options.progressBarHideCb) {
			eval( self.options.progressBarHideCb )();
		} else if (typeof self.options.progressBarSelector == 'undefined') {
			$('#'+this.progressBarId).addClass('hidden');
		} else {
			$(self.options.progressBarSelector).html(self.progressBarDefaultHtml);
		}
	}

	this.responseHandler = function(data)
	{
		self.hideProgressBar();

		if (data.refresh) {
			$(this.options.container).html(data.content);
		}
		else if (this.options.reverse) {
			$(this.options.container).prepend(data.content);
		} else {
			$(this.options.container).append(data.content);
		}
		this.nextPageUrl = data.nextPageUrl;

		if (typeof data.loadMoreLabel != 'undefined') {
			$('#' + this.options.linkContainerId+' a').html(data.loadMoreLabel);
		}

		if (this.nextPageUrl == false && (self.options.mode == 'load-more-link' || self.options.mode == 'load-more-scroll'))
			$('#' + this.options.linkContainerId).hide();

		// если включен режим загрузки по скроллу
		if (self.options.mode == 'load-more-scroll' || self.options.mode == 'load-more-scroll-floatpager') {
			// есть адрес следующей страницы - снимаем блокировку
			if (this.nextPageUrl)
				this.options.block = false;
		}
		appMain.initPopoverDefault();
		if (data && data.callback) {
			$.globalEval(data.callback);
		}
		appMain.initCalcEmulator();
	}

	this.getUrl = function()
	{
		var url;
		if (self.nextPageUrl == false) {
			url = '';
		}
		else {
			url = self.nextPageUrl;
			this.nextPageUrl = false;
		}
		return url;
	}

	/**
	 * перестроение постранички
	 */
	this.buildPagination = function()
	{
		// показать\скрыть плавающую постраничку
		if ($(window).scrollTop() > $('.' + self.options.floatPaginationItemClass + ':first').offset().top)
			$('.' + self.options.floatPaginationContainer).fadeIn(500);
		else
			$('.' + self.options.floatPaginationContainer).fadeOut(500);

		// текущая страница - минимльное значение, которое может принять пагинатор
		var page = self.options.currentPage;
		// обходим все первые и последние сообщения каждой страницы
		$.each($('.' + self.options.floatPaginationItemClass), function(index, value){
			// отсутп области видимости от начала документа
			var S = $(window).scrollTop();
			// отступ текущего сообщения от начала документа - отступ первого сообщения is_starter
			var top = $(value).offset().top - $('.' + self.options.floatPaginationItemClass + ':first').offset().top;
			if (S > top && $(value).data('page'))
				page = $(value).data('page');
		});
		// массив пунктов постранички
		var paginationButtons = $('.' + self.options.floatPaginationContainer + ' li.page');
		// проходим все кнопки постранички
		$.each(paginationButtons, function(index, value){
			// номер страницы
			var number = parseInt($('a', value).html());
			// если номер страницы постранички совпадает с текущей страницей
			if (number == page)
			{
				// делаем старый активный пункт неактивным
				$(paginationButtons).removeClass('selected');
				// этот - активным
				$(value).addClass('selected').removeClass('hidden');

				// скрываем и показываем нужные кноки
				var first = $('.' + self.options.floatPaginationContainer + ' li.first');
				if (page >= self.options.maxButtonCount)
					first.removeClass('hidden');
				else
					first.addClass('hidden');

				var last = $('.' + self.options.floatPaginationContainer + ' li.last');
				if (page <= self.options.pageCount - 1)
					last.removeClass('hidden');
				else
					last.addClass('hidden');

			}
			else if ((number < page - self.options.maxButtonCount) || (number > page + self.options.maxButtonCount))
				$(value).addClass('hidden');
			else if ((number >= page - self.options.maxButtonCount) && (number <= page + self.options.maxButtonCount))
				$(value).removeClass('hidden');
		});

		var pagination = '.' + self.options.floatPaginationContainer;
		$(pagination).parents().first().width($(pagination).width() + parseInt($(pagination).css('padding-left')) + parseInt($(pagination).css('padding-right')));
	}
}
var appWidgetLoadMore = new AppWidgetLoadMore();