function SettingsWidgetAgreements() {

	this.options = {}

	var self = this;

	this.init = function(options)
	{
		$('.js-affiliate-agreement-on').bind('change', function(e) {
			appMain.ajaxPost(this);
		});
	};

	this.initCreate = function()
	{
		$('.js-profile-link').on('input', function() {
			appMain.ajaxPost(this);
		});

		$('.js-percent').on('change', function(e) {
			appMain.ajaxPost(this);
		});
	}

}
var settingsWidgetAgreements = new SettingsWidgetAgreements();