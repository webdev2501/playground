<div class="float">
	<?php $form = $this->beginWidget(
		'CActiveForm', array(
		'htmlOptions' => array(
			'data-init' => 'dirty-check',
			'class' => 'form-horizontal',
		))) ?>
		<div class="b-row-reply-pm">
			<div class="b-reply-pm-input">
				<?= $form->textField($model, 'reply_pm_stop_words', array('class'=>'form-control', 'placeholder'=>'Example: Best regards, '.Yii::app()->user->model->fullname)) ?> <i class="help-hint-default fa fa-info-circle" data-placement="top" data-trigger="hover" data-container="body" data-content="To prevent entire email with all quotes to be pasted into this chat, put stop phrases here. Usually it is your signature. After this phrase, contents of email will be truncated."></i>
			</div>
			<div class="b-reply-pm-submit">
				<button type="submit" class="btn btn-primary ajax-post" data-href="<?= $this->controller->createUrl('wreplypm.save')?>" data-widget="wreplypm">Save</button>
			</div>
		</div>
	<?php $this->endWidget() ?>
</div>
<div class="clearfix"></div>