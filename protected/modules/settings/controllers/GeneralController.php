<?php
class GeneralController extends ModuleController
{
	public function init()
	{
		if (!parent::init()) return false;
		return true;
	}

	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
			'ajaxOnly + changePassword'
		));
	}

	public function actions()
	{
		return array_merge(
			parent::actions(),
			array(
				'wchangepassword.'=>array(
					'class'=>'application.modules.settings.components.widgets.wChangePassword.wChangePassword',
				),
				'wreplypm.'=>array(
					'class'=>'application.modules.settings.components.widgets.wReplyPm.wReplyPm',
				),
				'wopenafterlogin.'=>array(
					'class'=>'application.modules.settings.components.widgets.wOpenAfterLogin.wOpenAfterLogin',
				),
				'wmyprofilelink.'=>array(
					'class'=>'application.modules.settings.components.widgets.wMyProfileLink.wMyProfileLink',
				),
				'wlng.'=>array(
					'class'=>'application.modules.settings.components.widgets.wLng.wLng',
				),
			)
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionChangePassword()
	{
		$this->layoutContainer = '#modal_dialog_general .modal-content';
		$this->appendJsonResponse(array(
			'callback'=>array('appMain.openModal()')
		));
		$this->render('change_password');
	}
}