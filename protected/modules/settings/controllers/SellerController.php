<?php

class SellerController extends ModuleController
{
	public function init()
	{
		if (!parent::init()) return false;
		return true;
	}

	public function actions()
	{
		return array_merge(
			parent::actions(),
			array(
				'wniches.'=>[
					'class'=>'application.modules.settings.components.widgets.wNiches.wNiches',
				],
				'wextraprices.'=>[
					'class'=>'application.modules.settings.components.widgets.wExtraPrices.wExtraPrices',
				],
				'waffiliateagreements.'=>[
					'class'=>'application.modules.settings.components.widgets.wAffiliateAgreements.wAffiliateAgreements',
				],
				'wresellersettings.' => [
					'class' => 'application.modules.settings.components.widgets.wResellerSettings.wResellerSettings',
				],
				'affiliateagreement'=>array(
					'class'=>'application.modules.solos.components.actions.AffAgr',
				),
			)
		);
	}

	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
			'postOnly + delete, waffiliateagreements.save, waffiliateagreements.update, waffiliateagreements.cancel, affiliateagreement',
			'ajaxOnly + waffiliateagreements.save, waffiliateagreements.update, waffiliateagreements.cancel, affiliateagreement',
		));
	}

	public function actionIndex()
	{
		$model = ProfileSolo::model()->curUser()->find();
		$model->setScenario('settingsSellerGeneral');
		if (isset($_POST['ProfileSolo'])) {
			$model->attributes = $_POST['ProfileSolo'];

			if ($model->validate()) {
				$model->save(false);

				// update click price
				if ($model->click_price_min != $model->click_price_max) {
					$model->updateClickPrice();
				} else {
					$model->updateByPk($model->id, ['click_price'=>$model->click_price_min]);
				}

				if ($model->is_active==1 && $model->is_active!=$model->old_is_active) {
					$this->appendJsonResponse(array(
						'soft_redirect'=>$this->createUrl('seller/tips'),
					));
				}
				$this->appendJsonResponse(array(
					'callback'=>array(
						'appMain.showToast("Seller settings saved", "success")',
					),
				));

			} else {
				// включили, но возникла ошибка валидации
				if ($model->is_active==1 && $model->is_active!=$model->old_is_active) {
					$this->jsonResponse(array(
						'error'=>MyUtils::getFirstError($model),
						'callback'=>'settingsSeller.turnOnErrorCallback()',
					));
				} else {
					$this->appendJsonResponse(array(
						'error'=>MyUtils::getFirstError($model),
					));
				}
			}
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionTips()
	{
		$this->render('tips');
	}

	public function actionVerification($complete=false)
	{
		$model = new ListsizeVerifications();
		$model->scenario = 'create_verification';

		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];
			if ($model->validate()) {
				$resultCode = ListsizeVerifications::createRequests($model->idsAppRequest);

				if ($resultCode==ListsizeVerifications::RESULT_CODE_ERROR) {
					$this->jsonResponse(array(
						'error'=>'List size verification failed. Try to reconnect application'
					));

				} elseif (in_array($resultCode, array(ListsizeVerifications::RESULT_CODE_MANUAL, ListsizeVerifications::RESULT_CODE_BOTH))) {
					$this->jsonResponse(array(
						'soft_redirect'=>$this->createUrl('verification', array('complete'=>1))
					));
				} else {
					$this->jsonResponse(array(
						'callback'=>'appMain.showToast("List size verification complete", "success")',
						'soft_redirect'=>$this->createUrl('index')
					));
				}

			} else {
				$this->jsonResponse(array('error'=>MyUtils::getFirstError($model)));
			}
		}

		$this->render('verification', array(
			'model'=>$model,
			'apps'=>Apps::model()->activeAutoresponders()->newRequests(Yii::app()->user->id)->findAll(),
			//'appsVerified'=>AppsUsers::model()->with('idApp')->findAll(array('condition'=>'id_user=:id_user and list_size>0', 'params'=>array(':id_user'=> Yii::app()->user->id))),
			'appsVerified'=>AppsUsers::model()->with('idApp')->findAll([
				'condition'=>'id_user=:id_user AND api_params<>""',
				'params'=>[':id_user'=> Yii::app()->user->id],
			]),
			'complete'=>$complete,
		));
	}

	public function actionDelete($id)
	{
		$model = AppsUsers::model()->findByPk($id);

		if (empty($model)) {
			throw new CHttpException(404, 'Page not found');
		}

		$model->list_size = $model->list_size_bak = 0;
		$model->save(false);

		$model = Apps::model()->findByPk($model->id_app);
		$class = $model->class;
		$inst = new $class(Yii::app()->user->id);
		$inst->disconnect();

		$this->jsonResponse(array(
			'callback'=>'appMain.showToast("'.$model->name.' disconnected successful", "success")',
			'soft_redirect'=>$this->createUrl('verification'),
		));
	}
} 