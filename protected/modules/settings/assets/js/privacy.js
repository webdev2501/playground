function SettingsPrivacy() {

	this.options = {};

	var self = this;

	this.init = function()
	{
		$('.privacy-checkbox').bind('click', function(e) {
			if ($(this).hasClass('in-progress')) {
				e.stopPropagation();
				e.preventDefault();
				return false;
			}

			$('.privacy-checkbox').each(function() {
				$(this).addClass('in-progress');
			});

			$(this).parents('form').find('.js-privacy-submit').click();
		});

	}
}
var settingsPrivacy = new SettingsPrivacy();