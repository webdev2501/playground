<?php
class m170316_091542_install extends CDbMigration
{
	private $sql = [];

	public function up()
	{
		$this->sql[]=<<<SQL

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abr` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cnt_users` int(11) DEFAULT NULL,
  `is_top` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `abr` (`abr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_blocked` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_type` bigint(20) DEFAULT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type` (`id_type`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ord` (`ord`),
  KEY `sname` (`sname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dta` datetime NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_type` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_phpSubject` tinyint(4) NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_category` bigint(20) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `editable` tinyint(4) NOT NULL DEFAULT '1',
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_category` (`id_category`),
  KEY `active` (`active`),
  KEY `editable` (`editable`),
  KEY `ord` (`ord`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ip2_location` (
  `ipFROM` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  `ipTO` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  `countrySHORT` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countryLONG` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipREGION` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipCITY` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipLATITUDE` double DEFAULT NULL,
  `ipLONGITUDE` double DEFAULT NULL,
  `ipISP` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipDOMAIN` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipMCC` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipMNC` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipBRANDNAME` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipUSAGETYPE` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ipFROM`,`ipTO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ip2_location_spec` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipATON` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  `ip` varchar(32) NOT NULL,
  `countrySHORT` char(2) NOT NULL,
  `countryLONG` varchar(64) NOT NULL,
  `ipREGION` varchar(128) NOT NULL,
  `ipCITY` varchar(128) NOT NULL,
  `ipLATITUDE` double DEFAULT NULL,
  `ipLONGITUDE` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipATON` (`ipATON`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ip2_location_temp` (
  `ipFROM` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  `ipTO` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  `countrySHORT` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countryLONG` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipREGION` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipCITY` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipLATITUDE` double DEFAULT NULL,
  `ipLONGITUDE` double DEFAULT NULL,
  `ipISP` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipDOMAIN` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipMCC` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipMNC` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ipBRANDNAME` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ipUSAGETYPE` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ipFROM`,`ipTO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ip2_proxy` (
  `IP_FROM` bigint(20) NOT NULL,
  `IP_TO` bigint(20) NOT NULL,
  `COUNTRY_CODE` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `COUNTRY_NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IP_FROM`,`IP_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ip2_proxy_manual` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_clicks_log` bigint(20) DEFAULT NULL,
  `ip` varchar(32) NOT NULL,
  `ip_aton` bigint(20) NOT NULL,
  `ip_real` varchar(32) DEFAULT NULL,
  `ip_real_aton` bigint(20) DEFAULT NULL,
  `dta` datetime DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `test` tinyint(4) NOT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dta` (`dta`),
  KEY `id_clicks_log` (`id_clicks_log`),
  KEY `ip_real` (`ip_real`),
  KEY `ip_real_aton` (`ip_real_aton`),
  KEY `ip` (`ip`),
  KEY `ip_aton` (`ip_aton`),
  KEY `test` (`test`),
  KEY `source` (`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `ip2_proxy_temp` (
  `IP_FROM` bigint(20) NOT NULL,
  `IP_TO` bigint(20) NOT NULL,
  `COUNTRY_CODE` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `COUNTRY_NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IP_FROM`,`IP_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `listsize_verifications` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `id_app` bigint(20) DEFAULT NULL,
  `id_app_user` bigint(20) DEFAULT NULL,
  `old_value` int(11) NOT NULL,
  `new_value` int(11) NOT NULL,
  `create_dta` datetime DEFAULT NULL,
  `status` enum('new','complete') NOT NULL,
  `complete_result` enum('approved','denied','canceled') DEFAULT NULL,
  `complete_dta` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_app` (`id_app`),
  KEY `status` (`status`),
  KEY `create_dta` (`create_dta`),
  KEY `id_app_user` (`id_app_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `lng_source_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `message` text,
  `hash` varchar(255) DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `lng_translated_messages` (
  `id` bigint(20) NOT NULL,
  `language` varchar(16) NOT NULL,
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `logins_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `dta` datetime DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `id_country` int(11) DEFAULT NULL,
  `is_success` varchar(45) NOT NULL,
  `fingerprint` bigint(20) DEFAULT NULL,
  `fingerprint2` varchar(32) DEFAULT NULL,
  `usage_type` varchar(25) DEFAULT NULL,
  `proxy` tinyint(1) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `prev_distance` bigint(20) DEFAULT NULL,
  `prev_time_diff` int(11) DEFAULT NULL,
  `prev_id_country` bigint(20) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `time_zone_name` varchar(255) DEFAULT NULL,
  `time_zone_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `ip` (`ip`),
  KEY `is_success` (`is_success`),
  KEY `id_country` (`id_country`),
  KEY `fingerprint` (`fingerprint`),
  KEY `usage_type` (`usage_type`),
  KEY `fingerprint2` (`fingerprint2`),
  KEY `prev_distance` (`prev_distance`),
  KEY `prev_time_diff` (`prev_time_diff`),
  KEY `dta` (`dta`),
  KEY `prev_id_country` (`prev_id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `niche_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `id_niche` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_niche` (`id_niche`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `niches` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `default` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `default` (`default`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `param` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `user_identity_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `tp` enum('forgot_password','confirm_email','control_login','cookie_login','mobile_login') NOT NULL,
  `dta` datetime DEFAULT NULL,
  `key` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `id_user` (`id_user`),
  KEY `dta` (`dta`),
  KEY `tp` (`tp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `users_online` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) DEFAULT NULL,
  `is_online` int(11) NOT NULL,
  `dta_login` datetime DEFAULT NULL,
  `dta_logout` datetime DEFAULT NULL,
  `ip` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_aton` bigint(20) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `is_custom_location` tinyint(1) DEFAULT '0',
  `country` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_resource` (`is_online`),
  KEY `dta_login` (`dta_login`),
  KEY `dta_logout` (`dta_logout`),
  KEY `ip` (`ip`),
  KEY `ip_aton` (`ip_aton`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `paypal_affiliate` varchar(255) NOT NULL,
  `id_ref` bigint(20) DEFAULT NULL,
  `id_ref_old` bigint(20) NOT NULL,
  `ref_type` tinyint(4) NOT NULL,
  `ref_lock` tinyint(4) DEFAULT '0',
  `dta_reg` datetime DEFAULT NULL,
  `ip_last_login` varchar(16) NOT NULL,
  `dta_first_success_solo` date DEFAULT NULL,
  `list_size` int(11) NOT NULL,
  `is_list_verify` tinyint(4) NOT NULL,
  `list_size_unverify` int(11) NOT NULL,
  `avatar_ext` varchar(255) NOT NULL,
  `uid` varchar(5) NOT NULL,
  `uid_profile` varchar(32) NOT NULL,
  `uid_profile_date` datetime DEFAULT NULL,
  `hash` varchar(16) DEFAULT NULL,
  `accept_in` int(11) NOT NULL,
  `seller_rep` int(11) NOT NULL,
  `seller_rate_success` int(11) NOT NULL,
  `seller_rate_fail` int(11) NOT NULL,
  `seller_rate_success_arc` int(11) NOT NULL,
  `seller_rate_fail_arc` int(11) NOT NULL,
  `buyer_rep` int(11) NOT NULL,
  `buyer_rate_success` int(11) NOT NULL,
  `buyer_rate_fail` int(11) NOT NULL,
  `buyer_rate_success_arc` int(11) NOT NULL,
  `buyer_rate_fail_arc` int(11) NOT NULL,
  `rate_fail_count` int(11) NOT NULL,
  `rate_fail_dta` datetime DEFAULT NULL,
  `is_award` tinyint(4) NOT NULL,
  `aboutme` varchar(255) NOT NULL,
  `silence_period` int(11) NOT NULL,
  `silence_until_dta` datetime DEFAULT NULL,
  `silence_reason` varchar(255) DEFAULT NULL,
  `is_confirmed` tinyint(4) NOT NULL,
  `affiliate_popover_dta` datetime DEFAULT NULL,
  `is_aff_payment_made` tinyint(4) NOT NULL,
  `promoted_dta` datetime DEFAULT NULL,
  `power_dta` datetime DEFAULT NULL,
  `isSoundNewMessage` tinyint(4) NOT NULL DEFAULT '1',
  `isSoundNewProposal` tinyint(4) NOT NULL DEFAULT '1',
  `notes` text,
  `is_monitored_topup` tinyint(4) NOT NULL,
  `is_banned_buyer` tinyint(4) NOT NULL,
  `is_ban_paypal` tinyint(4) NOT NULL,
  `is_ban_stripe` tinyint(4) NOT NULL,
  `is_idle_notify_sent` tinyint(1) DEFAULT '0',
  `score` bigint(20) NOT NULL,
  `score_member` bigint(20) NOT NULL,
  `score_last` bigint(20) NOT NULL,
  `orders_cnt` int(11) NOT NULL,
  `orders_clicks` int(11) NOT NULL,
  `id_tsource` bigint(20) DEFAULT NULL,
  `dta_tsource` date DEFAULT NULL,
  `reply_pm_stop_words` varchar(255) NOT NULL,
  `tech` varchar(255) NOT NULL,
  `sc_greybox` tinyint(4) NOT NULL DEFAULT '0',
  `response_ratings_cnt` int(5) DEFAULT '0',
  `response_ratings_avg` decimal(3,1) NOT NULL DEFAULT '0.0',
  `lang` varchar(2) DEFAULT NULL,
  `cpa_api_key` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `uid` (`uid`),
  UNIQUE KEY `uid_profile` (`uid_profile`),
  UNIQUE KEY `hash_UNIQUE` (`hash`),
  KEY `role` (`role`),
  KEY `dta_reg` (`dta_reg`),
  KEY `list_size` (`list_size`),
  KEY `is_list_verify` (`is_list_verify`),
  KEY `fullname` (`fullname`),
  KEY `accept_in` (`accept_in`),
  KEY `seller_rate_success` (`seller_rate_success`),
  KEY `seller_rate_fail` (`seller_rate_fail`),
  KEY `silence_period` (`silence_period`),
  KEY `silence_until_dta` (`silence_until_dta`),
  KEY `dta_first_success_solo` (`dta_first_success_solo`),
  KEY `ip_last_login` (`ip_last_login`),
  KEY `id_ref` (`id_ref`),
  KEY `promoted_dta` (`promoted_dta`),
  KEY `is_monitored_topup` (`is_monitored_topup`),
  KEY `is_banned_buyer` (`is_banned_buyer`),
  KEY `score` (`score`),
  KEY `score_member` (`score_member`),
  KEY `orders_cnt` (`orders_cnt`),
  KEY `id_tsource` (`id_tsource`),
  KEY `is_ban_paypal` (`is_ban_paypal`),
  KEY `power_dta` (`power_dta`),
  KEY `rate_fail_count` (`rate_fail_count`),
  KEY `rate_fail_dta` (`rate_fail_dta`),
  KEY `tech` (`tech`),
  KEY `score_last` (`score_last`),
  KEY `is_ban_stripe` (`is_ban_stripe`),
  KEY `dta_tsource` (`dta_tsource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `profile_buyer` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `sift_score` decimal(5,2) DEFAULT NULL,
  `sift_label` varchar(255) DEFAULT NULL,
  `sift_reasons` text,
  `is_sift_import` tinyint(4) NOT NULL,
  `is_baned_brokering` tinyint(4) NOT NULL,
  `broker_api_key` varchar(255) DEFAULT NULL,
  `solos_total_cnt` int(11) NOT NULL,
  `refunds_beforedeliv_cnt` int(11) NOT NULL,
  `refunds_afterdeliv_cnt` int(11) NOT NULL,
  `is_was_penalty` tinyint(4) DEFAULT NULL,
  `privacy_hide_mas` tinyint(4) NOT NULL,
  `privacy_hide_news_rate` tinyint(4) NOT NULL,
  `privacy_hide_news_buy` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `broker_api_key` (`broker_api_key`),
  KEY `sift_score` (`sift_score`),
  KEY `id_user` (`id_user`),
  KEY `sift_label` (`sift_label`),
  KEY `is_sift_import` (`is_sift_import`),
  KEY `is_brokering` (`is_baned_brokering`),
  KEY `is_was_penalty` (`is_was_penalty`),
  KEY `privacy_hide_mas` (`privacy_hide_mas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `profile_solo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `clicks_max` int(11) NOT NULL,
  `clicks_min` int(11) NOT NULL,
  `clicks_max_daily` int(11) NOT NULL,
  `click_price` decimal(9,2) NOT NULL,
  `click_price_min` decimal(9,2) NOT NULL,
  `click_price_max` decimal(9,2) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_baned` tinyint(4) NOT NULL,
  `sell_mon` tinyint(4) NOT NULL,
  `sell_tue` tinyint(4) NOT NULL,
  `sell_wed` tinyint(4) NOT NULL,
  `sell_thu` tinyint(4) NOT NULL,
  `sell_fri` tinyint(4) NOT NULL,
  `sell_sat` tinyint(4) NOT NULL,
  `sell_sun` tinyint(4) NOT NULL,
  `overdelivery_percent` int(11) NOT NULL,
  `odf_sendto` enum('buyer','seller') NOT NULL,
  `odf_url` varchar(255) DEFAULT NULL,
  `is_cpa` tinyint(4) NOT NULL DEFAULT '1',
  `locked_days` longtext NOT NULL,
  `solos_number` varchar(45) NOT NULL,
  `max_clicks` int(11) NOT NULL,
  `order_delay` int(11) NOT NULL,
  `order_ttl` int(11) NOT NULL,
  `is_kent` tinyint(4) NOT NULL,
  `kent_percent` int(11) NOT NULL,
  `kent_note` varchar(255) NOT NULL,
  `extra_price_swipe` decimal(9,2) DEFAULT NULL,
  `extra_price_squeeze` decimal(9,2) DEFAULT NULL,
  `extra_price_custom_name1` varchar(64) DEFAULT NULL,
  `extra_price_custom_value1` decimal(9,2) DEFAULT NULL,
  `sales_cnt` int(11) NOT NULL,
  `sales_clicks` varchar(45) NOT NULL,
  `refund_rate` decimal(10,2) NOT NULL,
  `total_cnt` bigint(20) NOT NULL,
  `refunded_cnt` bigint(20) NOT NULL,
  `id_solo_sales` bigint(20) DEFAULT NULL,
  `sales_cnt_total` int(10) DEFAULT NULL,
  `hot_sales_cnt` int(11) DEFAULT NULL,
  `hot_dta` datetime DEFAULT NULL,
  `before_order_form` text,
  `after_order_form` text,
  `best_solo_timezone` varchar(3) DEFAULT NULL,
  `best_solo_time` varchar(255) DEFAULT NULL,
  `search_handicap` tinyint(4) NOT NULL,
  `proposals_cnt` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `clicks_min` (`clicks_min`),
  KEY `clicks_max` (`clicks_max`),
  KEY `click_price` (`click_price`),
  KEY `sell_mon` (`sell_mon`),
  KEY `sell_tue` (`sell_tue`),
  KEY `sell_wed` (`sell_wed`),
  KEY `sell_thu` (`sell_thu`),
  KEY `sell_fri` (`sell_fri`),
  KEY `sell_sat` (`sell_sat`),
  KEY `sell_sun` (`sell_sun`),
  KEY `order_delay` (`order_delay`),
  KEY `is_baned` (`is_baned`),
  KEY `sales_cnt` (`sales_cnt`),
  KEY `refund_rate` (`refund_rate`),
  KEY `total_cnt` (`total_cnt`,`refunded_cnt`),
  KEY `overdelivery_percent` (`overdelivery_percent`),
  KEY `is_kent` (`is_kent`),
  KEY `id_solo_sales` (`id_solo_sales`),
  KEY `hot_dta` (`hot_dta`),
  KEY `order_ttl` (`order_ttl`),
  KEY `search_handicap` (`search_handicap`),
  KEY `proposals_cnt` (`proposals_cnt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `affiliate_agreements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_seller` bigint(20) NOT NULL,
  `id_partner` bigint(20) NOT NULL,
  `percent` int(11) NOT NULL,
  `uid` char(16) NOT NULL DEFAULT '',
  `dta_create` datetime NOT NULL,
  `dta_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `id_seller` (`id_seller`),
  KEY `id_partner` (`id_partner`),
  KEY `dta_create` (`dta_create`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `email_blacklist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `create_dta` datetime DEFAULT NULL,
  `is_soft` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `FK_email_blacklist_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
		$this->executeSql();
	}

	public function down()
	{
		echo "m170316_091542_install does not support migration down.\\n";
		return false;
	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}