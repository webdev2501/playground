(function ($) {

	var methods = {
			init: function (options) {

				return this.each(function () {

					var $this = $(this),
						data = $this.data('switcher')

					// if plugin does not init yet
					if (!data) {

						// execute initialization

						$this.addClass('jquery-switcher-container');

						var
							fingerUp = $('<span />', {
								class : 'ud-thumbs-up',
								'data-value' : 'success',
							}),
							fingerDown = $('<span />', {
								class : 'ud-thumbs-down-flipped',
								'data-value' : 'fail'
							});

						$this.append(fingerDown).append(fingerUp);


						$this.find('input').each(function (index) {
							if (index == 0) {
								if ($(this).attr('data-label')) {
									fingerUp.append($('<span />', {
										class : 'sub-label',
										'html' : $(this).attr('data-label')
									}));
								}
								$(this).attr('data-value', 'success');
							} else if (index == 1) {
								if ($(this).attr('data-label')) {
									fingerDown.append($('<span />', {
										class : 'sub-label',
										'html' : $(this).attr('data-label')
									}));
								}
								$(this).attr('data-value', 'fail');
							}
							if ($(this).prop("checked")) {
								$(this).siblings('span[data-value='+($(this).data('value'))+']').addClass('checked');
							}
						});


						$this.find('span').bind('click.switcher', function() {
							// from all remove
							$this.find('span').removeClass('checked');
							$this.find('input').prop("checked", false);

							// for current apply
							$(this).addClass('checked');
							$this.find('input[data-value='+$(this).data('value')+']').prop("checked", true);
							$this.trigger('state-change', $(this).data('value'))
						});

						$(this).data('switcher', {
							target: $this
						});

					}
				});
			},
			destroy: function () {

				return this.each(function () {

					var $this = $(this),
						data = $this.data('switcher');

					$this.find('span').unbind('.switcher');
					data.switcher.remove();
					$this.removeData('switcher');

				})

			}
		};

	$.fn.switcher = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Methos whis name ' + method + ' does not exist for jQuery.switcher');
		}

	};

})
(jQuery);