﻿<!-- Dependencies -->
<script src="jquery.js" type="text/javascript"></script>

<!-- Core files -->
<script src="jquery.alerts.js" type="text/javascript"></script>
<link href="jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

<!-- Example script -->
<script type="text/javascript">
			
$(document).ready( function() {
				
$("#alert_button").click( function() {
jAlert('Это обычное окно предупреждения', 'Внимание !');
});
				
$("#confirm_button").click( function() {
jConfirm('Можете ли Вы подтвердить ?', 'Диалог подтверждения', function(r) {
jAlert('Подтверждение: ' + r, 'Результат подтверждения');
});
});
				
$("#prompt_button").click( function() {
jPrompt('Введите что-нибудь:', 'Ваш текст', 'Диалоговое окно', function(r) {
if( r ) alert('Вы ввели - ' + r);
});
});
				
$("#alert_button_with_html").click( function() {
jAlert('Вы можете использовать HTML, такие как <strong>жирный</strong>, <em>курсив</em>, и <u>подчёркнутый</u>!', 'Внимание !');
});
			
$(".alert_style_example").click( function() {
$.alerts.dialogClass = $(this).attr('id'); // set custom style class
jAlert('Это пользовательский стиль &ldquo;style_1&rdquo;', 'Свой стиль окна', function() {
$.alerts.dialogClass = null; // reset to default
});
});
});

</script>